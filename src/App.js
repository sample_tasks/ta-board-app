import React from 'react';
import MUITheme from "./muiTheme";
import Layout from "./pages/Layout";

export default function App() {

  return <MUITheme>
    <Layout />
  </MUITheme>
}
