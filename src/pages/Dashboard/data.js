import User1 from "../../images/dashboard/user1.svg";
import User2 from "../../images/dashboard/user2.svg";
import Angular from "../../images/dashboard/angular.svg";
import Java from "../../images/dashboard/java.svg";
import Python from "../../images/dashboard/python.svg";
import Uiux from "../../images/dashboard/uiux.svg";
import Candidate1 from '../../images/dashboard/candidate1.svg';
import Candidate2 from '../../images/dashboard/candidate2.svg';
import Candidate3 from '../../images/dashboard/candidate3.svg';
import profile1 from '../../images/dashboard/profile1.svg';
import profile2 from '../../images/dashboard/profile2.svg';
import profile3 from '../../images/dashboard/profile3.svg';

export const interviewDatas = [
    {
        "name": "John Smith",
        "role": "Senior Python Developer",
        "interview_date": "10th Jun 2024",
        "first_level_score": "2/10",
        "second_level_score": "7/10",
        "third_level_score": "waiting",
        "first_level_interviewer": "Stella",
        "second_level_interviewer": "Smith",
        "third_level_interviewer": "Stephan",
        "attendees": "Johnson",
        "meet_via": "G-Meet",
        "time": "3.30 P.M.",
        "profile": User1
    },
    {
        "name": "Jane Doe",
        "role": "Junior Python Developer",
        "interview_date": "12th Jun 2024",
        "first_level_score": "5/10",
        "second_level_score": "8/10",
        "third_level_score": "6/10",
        "first_level_interviewer": "Chris",
        "second_level_interviewer": "Morgan",
        "third_level_interviewer": "Taylor",
        "attendees": "Sam",
        "meet_via": "Zoom",
        "time": "3.30 P.M.",
        "profile": User2
    },
    {
        "name": "Mike Johnson",
        "role": "Data Scientist",
        "interview_date": "14th Jun 2024",
        "first_level_score": "9/10",
        "second_level_score": "8/10",
        "third_level_score": "waiting",
        "first_level_interviewer": "Emma",
        "second_level_interviewer": "Ethan",
        "third_level_interviewer": "Olivia",
        "attendees": "Noah",
        "meet_via": "Teams",
        "time": "3.30 P.M.",
        "profile": User1
    },
    {
        "name": "Sara Lee",
        "role": "Backend Developer",
        "interview_date": "15th Jun 2024",
        "first_level_score": "6/10",
        "second_level_score": "7/10",
        "third_level_score": "9/10",
        "first_level_interviewer": "Ava",
        "second_level_interviewer": "William",
        "third_level_interviewer": "Sophia",
        "attendees": "James",
        "meet_via": "WebEx",
        "time": "3.30 P.M.",
        "profile": User2
    },
    {
        "name": "David Brown",
        "role": "Full Stack Developer",
        "interview_date": "17th Jun 2024",
        "first_level_score": "8/10",
        "second_level_score": "8/10",
        "third_level_score": "7/10",
        "first_level_interviewer": "Benjamin",
        "second_level_interviewer": "Isabella",
        "third_level_interviewer": "Lucas",
        "attendees": "Mason",
        "meet_via": "G-Meet",
        "time": "3.30 P.M.",
        "profile": User1
    },
    {
        "name": "Emily Davis",
        "role": "Frontend Developer",
        "interview_date": "18th Jun 2024",
        "first_level_score": "7/10",
        "second_level_score": "6/10",
        "third_level_score": "5/10",
        "first_level_interviewer": "Mia",
        "second_level_interviewer": "Elijah",
        "third_level_interviewer": "Charlotte",
        "attendees": "Aiden",
        "meet_via": "Zoom",
        "time": "3.30 P.M.",
        "profile": User2
    },
    {
        "name": "Daniel Wilson",
        "role": "DevOps Engineer",
        "interview_date": "19th Jun 2024",
        "first_level_score": "6/10",
        "second_level_score": "9/10",
        "third_level_score": "waiting",
        "first_level_interviewer": "Amelia",
        "second_level_interviewer": "Jack",
        "third_level_interviewer": "Harper",
        "attendees": "Logan",
        "meet_via": "Teams",
        "time": "3.30 P.M.",
        "profile": User1
    },
    {
        "name": "Olivia Martinez",
        "role": "Machine Learning Engineer",
        "interview_date": "20th Jun 2024",
        "first_level_score": "5/10",
        "second_level_score": "8/10",
        "third_level_score": "waiting",
        "first_level_interviewer": "Aria",
        "second_level_interviewer": "Henry",
        "third_level_interviewer": "Scarlett",
        "attendees": "Alexander",
        "meet_via": "WebEx",
        "time": "3.30 P.M.",
        "profile": User2
    },
    {
        "name": "Liam Harris",
        "role": "AI Engineer",
        "interview_date": "21st Jun 2024",
        "first_level_score": "7/10",
        "second_level_score": "8/10",
        "third_level_score": "6/10",
        "first_level_interviewer": "Mason",
        "second_level_interviewer": "Lucas",
        "third_level_interviewer": "Isabella",
        "attendees": "Ella",
        "meet_via": "G-Meet",
        "time": "3.30 P.M.",
        "profile": User1
    },
    {
        "name": "Ava Clark",
        "role": "Data Analyst",
        "interview_date": "22nd Jun 2024",
        "first_level_score": "6/10",
        "second_level_score": "9/10",
        "third_level_score": "waiting",
        "first_level_interviewer": "Emily",
        "second_level_interviewer": "James",
        "third_level_interviewer": "Mia",
        "attendees": "Sophie",
        "meet_via": "Zoom",
        "time": "3.30 P.M.",
        "profile": User2
    }
]

export const postedJobs = [
    {
        active: [
            {
                id: "001",
                platform: "python",
                role: "Senior Developer",
                totalApplicants: 258,
                last_updated: "2024-06-10T08:24:00Z",
                time: "3 mins",
                value: 25,
                increased: true,
                weekChange: true,
                monthChange: false,
                platformImg: Python
            },
            {
                id: "002",
                platform: "java",
                role: "Junior Developer",
                totalApplicants: 180,
                last_updated: "2024-06-09T10:14:00Z",
                time: "7 mins",
                value: 30,
                increased: false,
                weekChange: false,
                monthChange: true,
                platformImg: Java
            },
            {
                id: "003",
                platform: "javascript",
                role: "Full Stack Developer",
                totalApplicants: 200,
                last_updated: "2024-06-08T12:00:00Z",
                time: "10 mins",
                value: 28,
                increased: false,
                weekChange: true,
                monthChange: true,
                platformImg: Uiux
            },
            {
                id: "004",
                platform: "ruby",
                role: "Backend Developer",
                totalApplicants: 175,
                last_updated: "2024-06-07T15:30:00Z",
                time: "2 mins",
                value: 22,
                increased: true,
                weekChange: false,
                monthChange: false,
                platformImg: Python
            }
        ]
    },
    {
        in_active: [
            {
                id: "005",
                platform: "python",
                role: "Data Scientist",
                totalApplicants: 90,
                last_updated: "2024-05-30T09:45:00Z",
                time: "3 mins",
                value: 40,
                increased: true,
                weekChange: true,
                monthChange: false,
                platformImg: Python
            },
            {
                id: "006",
                platform: "java",
                role: "Software Engineer",
                totalApplicants: 150,
                last_updated: "2024-05-25T11:20:00Z",
                time: "3 mins",
                value: 35,
                increased: true,
                weekChange: false,
                monthChange: true,
                platformImg: Java
            },
            {
                id: "007",
                platform: "python",
                role: "Senior Developer",
                totalApplicants: 124,
                last_updated: "2024-06-10T08:24:00Z",
                time: "3 mins",
                value: 25,
                increased: true,
                weekChange: true,
                monthChange: false,
                platformImg: Python
            },
            {
                id: "008",
                platform: "javascript",
                role: "Frontend Developer",
                totalApplicants: 130,
                last_updated: "2024-05-20T14:00:00Z",
                time: "8 mins",
                value: 18,
                increased: false,
                weekChange: false,
                monthChange: false,
                platformImg: Uiux
            }
        ]
    },
    {
        completed: [
            {
                id: "009",
                platform: "ruby",
                role: "DevOps Engineer",
                totalApplicants: 220,
                last_updated: "2024-04-15T16:45:00Z",
                time: "3 mins",
                value: 55,
                increased: false,
                weekChange: true,
                monthChange: true,
                platformImg: Angular
            },
            {
                id: "010",
                platform: "java",
                role: "System Analyst",
                totalApplicants: 190,
                last_updated: "2024-04-10T13:30:00Z",
                time: "2 mins",
                value: 50,
                increased: false,
                weekChange: false,
                monthChange: false,
                platformImg: Java
            },
            {
                id: "011",
                platform: "python",
                role: "Senior Developer",
                totalApplicants: 258,
                last_updated: "2024-06-10T08:24:00Z",
                time: "13 mins",
                value: 25,
                increased: true,
                weekChange: true,
                monthChange: false,
                platformImg: Python
            },
            {
                id: "012",
                platform: "javascript",
                role: "Full Stack Developer",
                totalApplicants: 205,
                last_updated: "2024-04-05T10:10:00Z",
                time: "3 mins",
                value: 60,
                increased: true,
                weekChange: true,
                monthChange: true,
                platformImg: Uiux
            }
        ]
    }
];

export const candidates = [
    {
        "job_id": "003",
        "name": "John Smith",
        "role": "Senior Python Developer",
        "interview_date": "10th Jun 2024",
        "first_level_score": "2/10",
        "second_level_score": "7/10",
        "third_level_score": "6/10",
        "fourth_level_score": "waiting",
        "total_marks": "waiting",
        "status": "Active"
    },
    {
        "job_id": "0032",
        "name": "Jane Doe",
        "role": "Junior Python Developer",
        "interview_date": "11th Jun 2024",
        "first_level_score": "8/10",
        "second_level_score": "7/10",
        "third_level_score": "6/10",
        "fourth_level_score": "5/10",
        "total_marks": "26/40",
        "status": "Hired"
    },
    {
        "job_id": "0503",
        "name": "Michael Brown",
        "role": "Python Developer",
        "interview_date": "12th Jun 2024",
        "first_level_score": "5/10",
        "second_level_score": "4/10",
        "third_level_score": "6/10",
        "fourth_level_score": "3/10",
        "total_marks": "18/40",
        "status": "Rejected"
    },
    {
        "job_id": "0034",
        "name": "Alice Johnson",
        "role": "Senior Python Developer",
        "interview_date": "13th Jun 2024",
        "first_level_score": "9/10",
        "second_level_score": "8/10",
        "third_level_score": "7/10",
        "fourth_level_score": "6/10",
        "total_marks": "30/40",
        "status": "Hired"
    },
    {
        "job_id": "053",
        "name": "Chris Lee",
        "role": "Junior Python Developer",
        "interview_date": "14th Jun 2024",
        "first_level_score": "4/10",
        "second_level_score": "4/10",
        "third_level_score": "5/10",
        "fourth_level_score": "5/10",
        "total_marks": "18/40",
        "status": "Rejected"
    },
    {
        "job_id": "303",
        "name": "Jessica Wilson",
        "role": "Python Developer",
        "interview_date": "15th Jun 2024",
        "first_level_score": "7/10",
        "second_level_score": "6/10",
        "third_level_score": "6/10",
        "fourth_level_score": "4/10",
        "total_marks": "23/40",
        "status": "Hired"
    },
    {
        "job_id": "053",
        "name": "David Martinez",
        "role": "Senior Python Developer",
        "interview_date": "16th Jun 2024",
        "first_level_score": "6/10",
        "second_level_score": "5/10",
        "third_level_score": "4/10",
        "fourth_level_score": "6/10",
        "total_marks": "21/40",
        "status": "Hired"
    },
    {
        "job_id": "006",
        "name": "Sarah Taylor",
        "role": "Junior Python Developer",
        "interview_date": "17th Jun 2024",
        "first_level_score": "5/10",
        "second_level_score": "5/10",
        "third_level_score": "5/10",
        "fourth_level_score": "5/10",
        "total_marks": "20/40",
        "status": "Rejected"
    },
    {
        "job_id": "013",
        "name": "Daniel Anderson",
        "role": "Python Developer",
        "interview_date": "18th Jun 2024",
        "first_level_score": "8/10",
        "second_level_score": "6/10",
        "third_level_score": "7/10",
        "fourth_level_score": "4/10",
        "total_marks": "25/40",
        "status": "Hired"
    },
    {
        "job_id": "503",
        "name": "Laura Thomas",
        "role": "Senior Python Developer",
        "interview_date": "19th Jun 2024",
        "first_level_score": "9/10",
        "second_level_score": "8/10",
        "third_level_score": "8/10",
        "fourth_level_score": "7/10",
        "total_marks": "32/40",
        "status": "Hired"
    },
    {
        "job_id": "007",
        "name": "Tom Garcia",
        "role": "Junior Python Developer",
        "interview_date": "20th Jun 2024",
        "first_level_score": "3/10",
        "second_level_score": "4/10",
        "third_level_score": "5/10",
        "fourth_level_score": "4/10",
        "total_marks": "16/40",
        "status": "Rejected"
    },
    {
        "job_id": "093",
        "name": "Emily Clark",
        "role": "Python Developer",
        "interview_date": "21st Jun 2024",
        "first_level_score": "6/10",
        "second_level_score": "7/10",
        "third_level_score": "6/10",
        "fourth_level_score": "5/10",
        "total_marks": "24/40",
        "status": "Hired"
    },
];

export const upcomings = [
    {
        date: "07 Feb",
        scope: "Interview with Designer",
        created_by: "stella",
        duration: "10 A.M to 11 A.M"
    },
    {
        date: "10 Feb",
        scope: "Technical Interview",
        created_by: "john",
        duration: "2 P.M to 3 P.M"
    },
    {
        date: "12 Feb",
        scope: "HR Interview",
        created_by: "mary",
        duration: "11 A.M to 12 P.M"
    },
    {
        date: "15 Feb",
        scope: "Final Round",
        created_by: "stella",
        duration: "1 P.M to 2 P.M"
    }
];

export const activity = [
    {
        name: "John Smith",
        role: "Python Developer",
        time: "15mins",
        interviewer: "stella",
        profile: Candidate1
    },
    {
        name: "Jane Doe",
        role: "Python Developer",
        time: "30mins",
        interviewer: "john",
        profile: Candidate2
    },
    {
        name: "Michael Brown",
        role: "Python Developer",
        time: "20mins",
        interviewer: "mary",
        profile: Candidate3
    },
    {
        name: "Alice Johnson",
        role: "Python Developer",
        time: "25mins",
        interviewer: "stella",
        profile: Candidate2
    }
];

export const hiringCanditate = [
    {
        name: "John Smith",
        descination: "Senior Python Developer",
        hired_by: "stella",
        profile: profile1
    },
    {
        name: "Jane Doe",
        descination: "Junior Python Developer",
        hired_by: "john",
        profile: profile2
    },
    {
        name: "Michael Brown",
        descination: "Python Developer",
        hired_by: "mary",
        profile: profile3
    },
    {
        name: "Alice Johnson",
        descination: "Senior Python Developer",
        hired_by: "stella",
        profile: profile1
    }
];
