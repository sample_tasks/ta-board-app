import React from "react";
import useStyles from "./styles";
import { Button } from "@mui/material";
import Girl from "../../images/dashboard/girl.svg";
import Document from "../../images/dashboard/document.svg";
import Graph from "../../components/Graph";
import InterviewData from "../../components/InterviewData";
import DateRange from "../../components/DateRange";
import { interviewDatas, postedJobs, candidates, upcomings, activity, hiringCanditate } from "./data";
import PostedJobs from "../../components/PostedJobs";
import CandidateTable from "../../components/CandidateTable";
import SimplerDatas from "../../components/SimplerDatas";

const Dashboard = () => {
    const { classes } = useStyles();

    return <div className={classes.contianer}>
        <div>
            <h3 className={classes.title}>
                HR Employee
            </h3>
            <span className={classes.spanContent}>
                Enjoy your selecting potential candidates Tracking and Management System.
            </span>
        </div>
        <div className={classes.graphContainer}>
            <div className={classes.graphLeftItem}>
                <Graph />
            </div>
            <div className={classes.graphRightItem}>
                <div className={classes.insideRightFirstItem}>
                    <div className={classes.documentContainer}>
                        <img src={Document} alt="document img" className={classes.document} />
                    </div>
                    <p className={classes.count}>0033</p>
                    <p className={classes.insideGraphSpan}>New Assessment&#39;s</p>
                </div>

                <div className={classes.insideRightSecondItem}>
                    <div className={classes.girlContainer}>
                        <img src={Girl} alt="girl1 img" className={classes.girl1} />
                        <img src={Girl} alt="girl2 img" className={classes.girl2} />
                    </div>
                    <div className={classes.btnContainer}>
                        <Button className={classes.btn}>VIEW DETAILS</Button>
                    </div>
                </div>
            </div>
        </div>
        <div className={classes.middleContainer}>
            <div className={classes.middleFirstContainer}>
                <InterviewData data={interviewDatas} />
                <PostedJobs data={postedJobs} />
                <CandidateTable data={candidates} />
            </div>
            <div className={classes.middleSecContainer}>
                <DateRange />
                <SimplerDatas
                    title="Upcomings"
                    data={upcomings}
                    btnClass={classes.viewDetailBtn1}
                    showBtn={true}
                />
                <SimplerDatas
                    title="Activity"
                    data={activity}
                    showBtn={false}
                />
                <SimplerDatas
                    title="Hiring Candidates"
                    data={hiringCanditate}
                    btnClass={classes.viewDetailBtn2}
                    showBtn={true}
                />
            </div>
        </div>
    </div>
}

export default Dashboard;