import { makeStyles } from "tss-react/mui";

const useStyles = makeStyles()((theme) => ({
    contianer: {
        display: 'flex',
        flexDirection: 'column'
    },
    title: {
        fontFamily: 'Poppins-Medium',
        fontSize: '18px'
    },
    spanContent: {
        fontSize: '14px'
    },
    graphContainer: {
        display: 'flex',
        flexFlow: 'row',
        marginTop: "25px",
        gap: '20px',
        [`${theme.breakpoints.down('sm')}`]: {
            flexFlow: 'column',
        },
    },
    graphLeftItem: {
        boxShadow: "4px 4px 25px 0px #00000026",
        height: '100%',
        flex: "1 1 0",
        minWidth: '48%',
        borderRadius: '10px'
    },
    graphRightItem: {
        background: "linear-gradient(74deg, #0A66C2 6.27%, #5994CE 90.96%)",
        boxShadow: "4px 4px 25px 0px #00000026",
        height: '100%',
        padding: '25px',
        borderRadius: '10px',
        [`${theme.breakpoints.up('sm')}`]: {
        },
        display: 'flex',
        flex: "1 1 0",
        minWidth: '48%'
    },
    insideRightFirstItem: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex-start',
        width: "50%"
    },
    insideRightSecondItem: {
        width: "50%",
        display: 'flex',
        flexDirection: 'column'
    },
    documentContainer: {
        background: "#FFFFFF",
        padding: '10px'
    },
    document: {
        width: '35px'
    },
    count: {
        fontSize: "30px",
        fontFamily: 'Poppins-SemiBold',
        color: '#FFFFFF'
    },
    insideGraphSpan: {
        fontSize: '12px',
        fontFamily: 'Poppins-Medium',
        color: '#FFFFFF'
    },
    girlContainer: {
        position: "relative",
        flexBasis: "75%",
        textAlign: "center"
    },
    btnContainer: {
        textAlign: 'right'
    },
    girl1: {
        width: '70px',
        marginLeft: '10%'
    },
    girl2: {
        height: "175px",
        position: "absolute",
        top: "-50px",
        left: "50%",
        [`${theme.breakpoints.down('sm')}`]: {
            left: "40%",
            width: '100%',

        },
    },
    btn: {
        fontFamily: 'Poppins-Medium',
        color: '#000000',
        fontSize: '14px',
        background: '#FFFFFF !important',
        borderRadius: '3px',
        border: 'none'
    },
    middleContainer: {
        display: 'flex',
        marginTop: '20px',
        gap: "20px",
        [`${theme.breakpoints.down('sm')}`]: {
            flexFlow: 'column'
        },
    },
    middleFirstContainer: {
        display: 'flex',
        flexDirection: 'column',
        flex: "1 1 0",
        flexBasis: '70%',
        maxWidth: 'calc(100% - 280px)',
        [`${theme.breakpoints.down('sm')}`]: {
            flexBasis: '100%',
            maxWidth: 'fit-content',
        },
    },
    middleSecContainer: {
        display: 'flex',
        flexDirection: 'column',
        flex: "1 1 0",
        maxWidth: '280px',
        [`${theme.breakpoints.down('sm')}`]: {
            flexBasis: '100%'
        },
    },
    viewDetailBtn1: {
        fontSize: "10px",
        background: "#0A66C2 !important",
        color: "#FFFFFF",
        borderRadius: '5px',
        height: 'fit-content',
        minWidth: 'fit-contnet',
        textTransform: 'capitalize'
    },
    viewDetailBtn2: {
        fontSize: "10px",
        background: "#FFFFFF !important",
        color: "#0A66C2",
        borderRadius: '5px',
        border: "1px solid #0A66C2",
        height: 'fit-content',
        minWidth: 'fit-contnet',
        textTransform: 'capitalize'
    },
}))

export default useStyles;