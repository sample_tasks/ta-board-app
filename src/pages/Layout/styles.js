
import { makeStyles } from "tss-react/mui";

const useStyles = makeStyles()((theme) => ({
    appBar: {
        boxShadow: "4px 4px 25px 0px #00000026",
        position: "fixed",
        top: 0,
        height: "50px",
        width: "100%",
        display: "flex",
        justifyContent: 'space-between',
        padding: "0px 15px 0px 5px",
        zIndex: "1000",
        backgroundColor: "#FFFFFF"
    },
    logo: {
        objectFit: 'contain',
        width: '150px'
    },
    searchIcon: {
        width: '45%'
    },
    adornment: {
        height: '100%',
        margin: '0px',
        maxHeight: 'fit-content',
        alignItems: 'unset'
    },
    logoContainer: {
        display: 'flex',
        alignItems: 'center'
    },
    icons: {
        width: '15px'
    },
    iconContainer: {
        display: 'flex',
        gap: '50px',
        alignItems: 'center',
        [`${theme.breakpoints.down('md')}`]: {
            gap: '10px',
        },
    },
    iconBtn: {
        padding: '7px'
    },
    totalContainer: {
        height: '100%',
        width: '100%',
        display: 'flex',
        paddingTop: '70px',
    },
    leftSideContainer: {
        height: '100%',
        borderRadius: "0px 20px 0px 0px",
        padding: "10px 20px",
        boxShadow: "4px 4px 25px 0px #00000026",
    },
    rightSideContainer: {
        flex: 1,
        height: '100%',
        padding: '20px',
        overflow: 'auto'
    },
    list: {
        listStyle: 'none',
        padding: '0px',
        margin: '0px',
        "& > li": {
            marginBottom: '20px'
        }
    },
    listIcon: {
        width: '20px'
    }
}));

export default useStyles;