import React from 'react';
import useStyles from './styles';
import Profile from "../../images/icons/profile.svg";
import Component1 from "../../images/icons/Component 1.svg";
import Component2 from "../../images/icons/Component 2.svg";
import Daymode from "../../images/icons/day-mode 1.svg";
import Notification from "../../images/icons/notification 1.svg";
import Surface1 from "../../images/icons/surface1.svg";
import Logo from "../../images/logo.svg";
import { IconButton, InputAdornment } from '@mui/material';
import SearchField from '../../components/SearchField';
import Search from "../../images/icons/search.svg";
import Group1 from "../../images/navbar/Group1.svg";
import Group2 from "../../images/navbar/Group2.svg";
import Group3 from "../../images/navbar/Group3.svg";
import Group4 from "../../images/navbar/Group4.svg";
import Group5 from "../../images/navbar/Group5.svg";
import Group6 from "../../images/navbar/Group6.svg";
import Group7 from "../../images/navbar/Group7.svg";
import Group8 from "../../images/navbar/Group8.svg";
import Group9 from "../../images/navbar/Group9.svg";
import Dashboard from '../Dashboard';

const Layout = () => {
    const { classes } = useStyles();
    return <>
        <div className={classes.appBar}>
            <div className={classes.logoContainer}>
                <img src={Logo} alt="logo" className={classes.logo} />
                <SearchField
                    placeholder="search"
                    adorments={{
                        endAdornment:
                            (<InputAdornment position="end" className={classes.adornment}>
                                <IconButton size='small'>
                                    <img src={Search} alt="search img" className={classes.searchIcon} />
                                </IconButton>
                            </InputAdornment>)
                    }}
                />
            </div>
            <div className={classes.iconContainer}>
                <IconButton size='small' className={classes.iconBtn}>
                    <img src={Daymode} alt="daymode img" className={classes.icons} />
                </IconButton>
                <IconButton size='small' className={classes.iconBtn}>
                    <img src={Surface1} alt="surface img" className={classes.icons} />
                </IconButton>
                <IconButton size='small' className={classes.iconBtn}>
                    <img src={Component1} alt="component 1 img" className={classes.icons} />
                </IconButton>
                <IconButton size='small' className={classes.iconBtn}>
                    <img src={Notification} alt="notification img" className={classes.icons} />
                </IconButton>
                <IconButton size='small' className={classes.iconBtn}>
                    <img src={Component2} alt="component 2 img" className={classes.icons} />
                </IconButton>
                <IconButton size='small' className={classes.iconBtn}>
                    <img src={Profile} alt="profile img" className={classes.icons} />
                </IconButton>
            </div>
        </div>
        <div className={classes.totalContainer}>
            <div className={classes.leftSideContainer}>
                <ul className={classes.list}>
                    <li>
                        <IconButton size="small">
                            <img src={Group1} alt='group1 img' className={classes.listIcon} />
                        </IconButton>
                    </li>
                    <li>
                        <IconButton size="small">
                            <img src={Group2} alt='group2 img' className={classes.listIcon} />
                        </IconButton>
                    </li>
                    <li>
                        <IconButton size="small">
                            <img src={Group3} alt='group3 img' className={classes.listIcon} />
                        </IconButton>
                    </li>
                    <li>
                        <IconButton size="small">
                            <img src={Group4} alt='group4 img' className={classes.listIcon} />
                        </IconButton>
                    </li>
                    <li>
                        <IconButton size="small">
                            <img src={Group5} alt='group5 img' className={classes.listIcon} />
                        </IconButton>
                    </li>
                    <li>
                        <IconButton size="small">
                            <img src={Group6} alt='group6 img' className={classes.listIcon} />
                        </IconButton>
                    </li>
                    <li>
                        <IconButton size="small">
                            <img src={Group7} alt='group7 img' className={classes.listIcon} />
                        </IconButton>
                    </li>
                    <li>
                        <IconButton size="small">
                            <img src={Group8} alt='group8 img' className={classes.listIcon} />
                        </IconButton>
                    </li>
                    <li>
                        <IconButton size="small">
                            <img src={Group9} alt='group9 img' className={classes.listIcon} />
                        </IconButton>
                    </li>
                </ul>
            </div>
            <div className={classes.rightSideContainer}>
                <Dashboard />
            </div>
        </div>
    </>
}

export default Layout;