import { makeStyles } from "tss-react/mui";

const useStyles = makeStyles()((theme) => ({
    postedJobsContianer: {
        overflow: 'auto',
        boxShadow: "4px 4px 25px 0px #00000026",
        width: '100%',
        marginBottom: '20px',
        borderRadius: '5px'
    },
    titleCotainer: {
        display: 'flex',
        padding: '10px 20px',
        justifyContent: 'space-between'
    },
    leftCotainer: {
        display: 'flex',
        alignItems: 'center',
        gap: '20px'
    },
    rightCotainer: {
        display: 'flex',
        alignItems: 'center',
        gap: '10px',
        justifyContent: 'flex-end'
    },
    title: {
        fontFamily: "Poppins-Medium",
        fontSize: '18px',
        color: "#000000"
    },
    linkBtn: {
        color: '#0A66C2',
        backgroundColor: "#FFFFFF !important",
        borderRadius: "0px",
        padding: '2px',
        textTransform: 'capitalize',
        textDecoration: "underline",
        fontSize: "12px"
    },
    iconBtn: {
        width: '20px',
        height: '20px',
    },
    searchIcon: {
        width: '45%'
    },
    adornment: {
        height: '100%',
        margin: '0px',
        maxHeight: 'fit-content',
        alignItems: 'unset'
    },
    filterImg: {
        width: '15px',
        marginRight: "5px"
    },
    filterBtn: {
        height: "35px",
        color: "#000000",
        fontSize: "10px",
        textTransform: 'capitalize',
        boxShadow: "4px 4px 25px 0px #00000026",
    },
    cardContainer: {
        padding: '20px',
        display: 'flex',
        gap: '15px'
    },
    cardItem: {
        boxShadow: "4px 4px 25px 0px #00000026",
        backgroundColor: "#FFFFFF",
        minWidth: '200px',
        display: "flex",
        flexDirection: "column",
        padding: '5px',
        flex: "1 1 0"
    },
    topItem: {
        display: "flex",
        fontSize: '12px',
        borderBottom: `1px solid ${theme.palette.divider}`,
        padding: '5px',
        justifyContent: 'space-between'
    },
    platformImg: {
        width: "20px"
    },
    topItemSpan: {
        color: "#091316"
    },
    midItem: {
        display: "flex",
        flexDirection: 'column',
        fontSize: '10px',
        borderBottom: `1px solid ${theme.palette.divider}`,
        padding: '10px',
        justifyContent: 'space-between',
        alignItems: "center",
        flex: 1
    },
    midItemSpan: {
        color: "#3E3E3E"
    },
    midItemDiv: {
        fontFamily: "Poppins-SemiBold",
        padding: '10px',
        width: '50px',
        height: '50px',
        display: 'flex',
        justifyContent: "center",
        alignItems: "center"
    },
    bottomItem: {
        display: 'flex',
        fontSize: "10px",
        justifyContent: "space-between",
        alignItems: "center",
        "& > p": {
            margin: "0px"
        }
    },
    aeroSpan: {
        color: "#0A66C2",
        fontSize: "18px"
    },
    blueSpan: {
        color: "#0A66C2",
        fontSize: "12px"
    },
    bottompara: {
        color: "#000000"
    }
}))

export default useStyles;