import React, { useState } from "react";
import useStyles from "./styles";
import { Button, InputAdornment, IconButton, Divider } from "@mui/material";
import SearchField from "../SearchField";
import Search from "../../images/icons/search.svg";
import Filter from "../../images/dashboard/filter.svg";
import TabsComponent from "../TabsComponent";
import { postedJobs } from "../../pages/Dashboard/data";

const PostedJobs = () => {
    const [selectedTab, setSelectedTab] = useState(0);
    const { classes } = useStyles();
    const categories = [
        {
            title: "Active Jobs",
            value: "active"
        },
        {
            title: "Inactive Jobs",
            value: "in_active"
        },
        {
            title: "Completed Jobs",
            value: "completed"
        },
    ];

    const boxColors = [
        {
            color: "#0A66C2",
            backgroundColor: "#D9E4EF"
        }, {
            backgroundColor: "#73A1FB",
            color: "#FFFFFF"
        }, {
            backgroundColor: "#2F73A0",
            color: "#FFFFFF"
        }, {
            backgroundColor: "#0A66C2",
            color: "#FFFFFF"
        }]

    const handleTabs = (e, tabIndex) => {
        setSelectedTab(tabIndex);
    }

    return <div className={classes.postedJobsContianer}>
        <div className={classes.titleCotainer}>
            <div className={classes.leftCotainer}>
                <span className={classes.title}>Posted Jobs</span>
                <Button className={classes.linkBtn}>View All</Button>
            </div>
            <div className={classes.rightCotainer}>
                <SearchField
                    placeholder="search"
                    adorments={{
                        endAdornment:
                            (<InputAdornment position="end" className={classes.adornment}>
                                <IconButton size='small'>
                                    <img src={Search} alt="search img" className={classes.searchIcon} />
                                </IconButton>
                            </InputAdornment>)
                    }}
                />
                <Button className={classes.filterBtn}>
                    <img src={Filter} alt="filter img" className={classes.filterImg} />
                    Filters
                </Button>
            </div>
        </div>
        <div style={{ marginTop: '20px' }}>
            <TabsComponent
                categories={categories}
                selectedTab={selectedTab}
                handleTabs={handleTabs}
                variant={"scrollable"}
                tabStyle={{
                    borderBottom: 1,
                    borderColor: 'divider',
                    '& .MuiButtonBase-root.MuiTab-root': {
                        padding: '5px 10px',
                    }
                }}
                paperStyle={{
                    padding: '0px 5px',
                    borderRadius: '20px 20px 0px 0px'
                }}
            >
                <div className={classes.cardContainer}>
                    {postedJobs[selectedTab] && categories[selectedTab] && postedJobs[selectedTab]?.[categories[selectedTab]?.value] ?
                        (postedJobs[selectedTab]?.[categories[selectedTab]?.value] || []).map((ele, i) => {
                            const randomNum = Math.floor(Math.random() * 4);
                            return (<div key={`${ele?.id}_${i}`} className={classes.cardItem}>
                                <div className={classes.topItem}>
                                    <img src={ele?.platformImg || ""} alt="platform img" className={classes.platformImg} />
                                    <span className={classes.topItemSpan}>{`${ele?.platform} Developers`}</span>
                                    <span className={classes.topItemSpan}>{ele?.id}</span>
                                </div>
                                <div className={classes.midItem}>
                                    <span className={classes.midItemSpan}>
                                        {ele?.role}
                                    </span>
                                    <div className={classes.midItemDiv} style={{
                                        color: boxColors[randomNum]?.color || "",
                                        backgroundColor: boxColors[randomNum]?.backgroundColor || ""
                                    }}>
                                        {ele?.totalApplicants}
                                    </div>
                                    <span className={classes.midItemSpan}>
                                        Total Applicants
                                    </span>
                                </div>
                                <div className={classes.bottomItem}>
                                    <p className={classes.bottompara}><span className={classes.aeroSpan}>{ele?.increased ? '\u2191' : '\u2193'}</span> <span className={classes.blueSpan}>{`${ele?.value}%`}</span> Vs Last{ele?.monthChange ? "month" : "week"}</p>
                                    <span>
                                        {ele?.time}
                                    </span>
                                </div>
                            </div>)
                        }) : null
                    }
                </div>
            </TabsComponent>
        </div>
    </div>
}

export default PostedJobs;