
import { makeStyles } from "tss-react/mui";

const useStyles = makeStyles()((theme) => ({
    interviewDataContainer: {
        overflow: 'auto',
        boxShadow: "4px 4px 25px 0px #00000026",
        width: '100%',
        marginBottom: '20px',
        padding: '10px 0px',
        borderRadius: '5px'
    },
    titleCotainer: {
        display: 'flex',
        padding: '10px 10px 10px 20px',
        justifyContent: 'space-between',
    },
    leftCotainer: {
        display: 'flex',
    },
    title: {
        fontFamily: "Poppins-Medium",
        fontSize: '16px',
        color: "#000000"
    },
    iconBtn: {
        width: '20px',
        height: '20px'
    },
    tableContainer: {
        overflow: "auto",
        display: 'flex',
        flexFlow: 'row',
        padding: '20px',
        gap: "20px"
    },
    table: {
        borderCollapse: 'collapse',
        borderRadius: '5px'
    },
    row: {
        height: '50px',
        verticalAlign: 'bottom',
        textAlign: 'center'
    },
    lastrow: {
        height: '70px'
    },
    tabledata: {
        border: "1px solid #D6D6D6",
        minWidth: '100px',
        verticalAlign: 'middle',
        fontSize: '12px'
    },
    tabledata1: {
        border: "1px solid #D6D6D6",
        minWidth: "180px",
        fontSize: '12px'
    },
    tabledataLast: {
        border: "1px solid #D6D6D6",
        minWidth: '120px',
        verticalAlign: 'middle',
        fontSize: '12px',
        textAlign: 'center'
    },
    profileContianer: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    profileimg: {
        width: "80px",
        height: '80px',
        borderRadius: '50%',
        border: "1px solid #0A66C2",
        objectFit: 'contain'
    },
    roleContainer: {
        display: 'flex',
        flexFlow: 'column',
        marginTop: '20px'
    },
    name: {
        color: '#091316',
        fontSize: '14px'
    },
    roleName: {
        color: '#091316',
        fontSize: '12px'
    },
    dateTimeContainer: {
        display: 'flex',
        flexFlow: 'column',
        alignItems: 'center'
    },
    dateTimeImg: {
        width: '20px'
    },
    dateTime: {
        color: '#0A66C2'
    },
    firstBtn: {
        border: "1px solid #0A66C2",
        color: "#0A66C2",
        backgroundColor: "#FFF !important",
        fontSize: '12px',
        textTransform: 'capitalize'
    },
    secondBtn: {
        backgroundColor: "#0A66C2 !important",
        color: "#FFFFFF",
        border: 'none',
        fontSize: '12px',
        textTransform: 'capitalize',
        marginLeft: "20px"
    }
}))

export default useStyles;