import React from "react";
import useStyles from "./styles";
import { IconButton, Divider, Button } from "@mui/material";
import Calendar from "../../images/dashboard/calendar.svg";
import Clock from "../../images/dashboard/clock.svg";

const InterviewData = (props) => {
    const { data } = props;
    const { classes } = useStyles();

    return <div className={classes.interviewDataContainer}>
        <div className={classes.titleCotainer}>
            <div className={classes.leftCotainer}>
                <span className={classes.title}>Today Interviews Meetings Info</span>
            </div>
            <IconButton size="small" className={classes.iconBtn}>
                <span style={{ color: '#091316', fontSize: '12px' }}>:</span>
            </IconButton>
        </div>
        <Divider sx={{ margin: "0px 5px" }} />
        <div className={classes.tableContainer}>
            {(data || []).map((ele, i) => <table className={classes.table} key={`${i}-${ele?.name}`}>
                <tbody>
                    <tr className={classes.row}>
                        <td colSpan={2} rowSpan={5} className={classes.tabledata}>
                            <div className={classes.profileContianer}>
                                <img src={ele?.profile} alt="profile img" className={classes.profileimg} />
                                <div className={classes.roleContainer}>
                                    <span className={classes.name}>{ele?.name}</span>
                                    <span className={classes.roleName}>{ele?.role}</span>
                                </div>
                            </div>
                        </td>
                        <td
                            className={classes.tabledata1}>
                            {`1st Level: ${ele?.first_level_score}`}
                        </td>
                        <td
                            className={classes.tabledata1}>{`Interviewer: ${ele?.first_level_interviewer}`}</td>
                    </tr>
                    <tr className={classes.row}>
                        <td
                            className={classes.tabledata1}>{`2nd Level: ${ele?.second_level_score}`}</td>
                        <td
                            className={classes.tabledata1}>{`Interviewer: ${ele?.second_level_interviewer}`}</td>
                    </tr>
                    <tr className={classes.row}>
                        <td
                            className={classes.tabledata1}>{`3rd Level: ${ele?.third_level_score}`}</td>
                        <td
                            className={classes.tabledata1}>{`Interviewer: ${ele?.third_level_interviewer}`}</td>
                    </tr>
                    <tr className={classes.row}>
                        <td
                            className={classes.tabledata1}>{`Meet Via: ${ele?.meet_via}`}</td>
                        <td
                            className={classes.tabledata1}>{`Attendees: ${ele?.attendees}`}</td>
                    </tr>
                    <tr style={{ height: '10px' }}>
                        <td
                            colSpan={2}
                            rowSpan={2} className={classes.tabledataLast}>
                            <Button className={classes.firstBtn}>Reschedule Meeting</Button>
                            <Button className={classes.secondBtn}>Join Meeting</Button>
                        </td>
                    </tr>
                    <tr className={classes.lastrow}>
                        <td className={classes.tabledata}>
                            <div className={classes.dateTimeContainer}>
                                <img src={Calendar} alt="interviewtime" className={classes.dateTimeImg} />
                                <span className={classes.dateTime}>{ele?.interview_date}</span>
                            </div>
                        </td>
                        <td className={classes.tabledata}>
                            <div className={classes.dateTimeContainer}>
                                <img src={Clock} alt="interviewtime" className={classes.dateTimeImg} />
                                <span className={classes.dateTime}>{ele?.time}</span>
                            </div>
                        </td>

                    </tr>
                </tbody>
            </table>)}

        </div>
    </div>
}

export default InterviewData