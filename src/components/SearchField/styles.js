import { makeStyles } from "tss-react/mui";
import { spanColor } from '../../utils/tools';

const useStyles = makeStyles()((theme) => ({
    searchContainer: {
        width: '55%',
        position: 'relative'
    },
    textField: {
        '& .MuiOutlinedInput-root': {
            borderRadius: '5px',
            height: 35,
            border: 'none',
            boxShadow: "4px 4px 25px 0px #00000026",
            ':hover': {
                border: 'none !important'
            },
            ':focus-within': {
                border: 'none !important'
            }
        },
        '& .MuiOutlinedInput-notchedOutline': {
            border: 'none'
        },
        input: {
            padding: "10px",
            color: spanColor,
            fontSize: '14px'
        }
    },
    inputs: {
        "&::placeholder": {
            fontWeight: 400,
            color: '#000000',
            opacity: "0.2px !important",
        },
    },
    inputLabel: {
        display: 'flex'
    },
    helperText: {
        fontSize: '12px',
        color: 'red',
        margin: '0px 18px',
    },
    amountField: {
        boxSizing: 'border-box',
    },
}))

export default useStyles;