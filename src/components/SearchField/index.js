import React, { useState } from 'react';
import useStyles from './styles';
import { TextField } from '@mui/material';

export default function SearchField(props) {
    const { name, placeholder, adorments } = props;
    const [text, setText] = useState('');
    const [displayText, setDisplayText] = useState('');
    const { classes } = useStyles();

    const handleSubmit = e => {
        e.preventDefault();
        setDisplayText(text);
    };

    const handleChange = e => {
        setText(e.target.value);
    };

    return (
        <div className={classes.searchContainer}>
            <TextField
                fullWidth
                InputLabelProps={{
                    shrink: true,
                }}
                autoComplete='off'
                type={"text"}
                InputProps={Object.assign({
                    classes: { input: classes.inputs },
                    className: classes.amountField,
                    placeholder: placeholder,

                }, { ...adorments })}
                name={name}
                className={classes.textField}
                onChange={handleChange}
                variant={"outlined"}
            />
        </div>
    );
}