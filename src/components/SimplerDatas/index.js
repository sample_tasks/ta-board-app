import React from "react";
import useStyles from "./styles";
import { Button } from "@mui/material";

const SimplerDatas = (props) => {
    const { title, btnClass, data, showBtn } = props;
    const { classes } = useStyles();
    const randomColors = [
        {
            color: "#0B74AD",
            background: "#0B74AD26"
        }, {
            color: "#091316",
            background: "#C0C1C1"
        },
        {
            color: "#091316",
            background: "#E1E1E1"
        }];

    return <div className={classes.totalContainer}>
        <div className={classes.topContainer}>
            <span className={classes.title}>{title}</span>
            <Button className={classes.viewBtn}>View All</Button>
        </div>
        <div className={classes.bottomContainer}>
            {(data || []).map((ele, i) => {
                const randomNum = Math.floor(Math.random() * 3);
                return (<div
                    key={`${i}_${Math.random()}`}
                    className={classes.bottomItem}>
                    {ele?.profile ?
                        <img src={ele?.profile || ""} alt="profile img" className={classes.profileItem} />
                        :
                        <div className={classes.leftItem} style={{
                            color: randomColors[randomNum]?.color || "",
                            backgroundColor: randomColors[randomNum]?.background || ""
                        }}>
                            <span>{ele?.date}</span>
                        </div>}
                    <div className={classes.middleItem}>
                        <span className={classes.middleFirst}>
                            {ele?.name ? (
                                ele?.role ? (
                                    <>
                                        {ele?.name} <span style={{ color: "#0B74AD" }}>{`[${ele?.role}]`}</span>
                                    </>
                                ) : (
                                    ele?.name
                                )
                            ) : (
                                ele?.scope
                            )}
                        </span>
                        <span className={classes.middleSec}>
                            {ele?.created_by ? (
                                <span>
                                    Created by <span style={{ color: "#0B74AD" }}>{ele?.created_by}</span>
                                </span>
                            ) : ele?.interviewer ? (
                                `Interview with ${ele?.interviewer}`
                            ) : (
                                ele?.descination
                            )}
                        </span>
                        <span className={classes.middleThird}>
                            {ele?.hired_by ? `Hired by: ${ele?.hired_by}` : ele?.time ? `${ele?.time} ago` : ele?.duration}
                        </span>
                    </div>
                    {showBtn ? <Button className={btnClass}>
                        Details
                    </Button> : null}
                </div>)
            })}
        </div>
    </div>
}

export default SimplerDatas;