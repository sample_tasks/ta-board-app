import { makeStyles } from "tss-react/mui";

const useStyles = makeStyles()((theme) => ({
    totalContainer: {
        display: 'flex',
        flexFlow: 'column',
        marginTop: '30px'
    },
    topContainer: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    title: {
        fontSize: "16px",
    },
    viewBtn: {
        textDecoration: 'underline',
        fontSize: "14px",
        textTransform: 'capitalize',
        color: "#0A66C2"
    },
    bottomContainer: {
        marginTop: '30px',
        display: 'flex',
        flexFlow: 'column',
        gap: '30px'
    },
    bottomItem: {
        display: 'flex',
        flexFlow: 'row'
    },
    leftItem: {
        width: '45px',
        height: '45px',
        fontFamily: "Poppins-Medium",
        fontSize: '13px',
        padding: '7px',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: '5px'
    },
    profileItem: {
        width: '45px',
        height: '45px',
        objectFit: 'contain',
        borderRadius: "50%",
        flexBasis: '10%'
    },
    middleItem: {
        display: 'flex',
        flexFlow: "column",
        flexBasis: '70%',
        marginLeft: '10px',
        gap: '5px'
    },
    middleFirst: {
        fontSize: '10px'
    },
    middleSec: {
        fontSize: '9px'
    },
    middleThird: {
        fontSize: '8px'
    },

}))

export default useStyles;