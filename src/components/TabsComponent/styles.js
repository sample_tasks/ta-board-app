import { makeStyles } from "tss-react/mui";

const useStyles = makeStyles()((theme) => ({
    paper: {
        flexGrow: 0,
        boxShadow: 'none',
    },
    tabs: {
        minHeight: 'fit-content',
        '& .MuiTabs-indicator': {
            backgroundColor: "#FFFFFF"
        },
        '& .MuiButtonBase-root.MuiTab-root': {
            minWidth: 'unset',
            minHeight: 'fit-content',
            color: '#3E3E3E',
            fontSize: '12px',
            fontFamily: 'Poppins-Medium',
            transition: 'color 0.2s ease-in-out',
            textTransform: 'capitalize',
            '&.Mui-selected': {
                color: "#3E3E3E",
                backgroundColor: "#DBEAF3"
            },
        },
    },
    categoryChildren: {
        marginTop: '20px'
    }
}));

export default useStyles;