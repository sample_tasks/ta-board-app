import React from "react";
import { Tabs, Tab, Paper } from "@mui/material";
import useStyles from "./styles";

const TabsComponent = (props) => {
    const { categories, paperStyle, variant, tabStyle, children, selectedTab, setSelectedTab, handleTabs } = props;
    const { classes } = useStyles();

    return <>
        <Paper square className={classes.paper} sx={paperStyle || {}}>
            <Tabs
                value={selectedTab}
                onChange={handleTabs}
                className={classes.tabs}
                variant={variant || "fullWidth"}
                scrollButtons={false}
                sx={tabStyle || {}}
                indicatorColor="primary"
                textColor="primary">
                {(categories || []).map((category, index) => <Tab key={index} label={category && category.title} />)}
            </Tabs>
        </Paper>
        <div>
            {children}
        </div>
    </>
}

export default TabsComponent;

