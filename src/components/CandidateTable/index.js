import React from "react";
import useStyles from "./styles";
import Eye from "../../images/dashboard/eye.svg"
import { Button, InputAdornment, IconButton } from "@mui/material";
import SearchField from "../SearchField";
import Search from "../../images/icons/search.svg";
import Filter from "../../images/dashboard/filter.svg";

const CandidateTable = (props) => {
    const { data } = props;
    const { classes } = useStyles();

    return <div className={classes.postedJobsContianer}>
        <div className={classes.titleCotainer}>
            <div className={classes.leftCotainer}>
                <span className={classes.title}>Posted Jobs</span>
                <Button className={classes.linkBtn}>View All</Button>
            </div>
            <div className={classes.rightCotainer}>
                <SearchField
                    placeholder="search"
                    adorments={{
                        endAdornment:
                            (<InputAdornment position="end" className={classes.adornment}>
                                <IconButton size='small'>
                                    <img src={Search} alt="search img" className={classes.searchIcon} />
                                </IconButton>
                            </InputAdornment>)
                    }}
                />
                <Button className={classes.filterBtn}>
                    <img src={Filter} alt="filter img" className={classes.filterImg} />
                    Filters
                </Button>
            </div>
        </div>
        <div className={classes.tableContainer}>
            <table className={classes.table}>
                <thead>
                    <tr className={classes.theadrow}>
                        <th className={classes.theaddata}>Job ID</th>
                        <th className={classes.theaddata}>Name</th>
                        <th className={classes.theaddata}>Position</th>
                        <th className={classes.theaddata}>1st Level</th>
                        <th className={classes.theaddata}>2nd Level</th>
                        <th className={classes.theaddata}>3rd Level</th>
                        <th className={classes.theaddata}>4th Level</th>
                        <th className={classes.theaddata}>Total Marks</th>
                        <th className={classes.theaddata}>Status</th>
                        <th className={classes.theaddata}>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {(data || []).map((ele, i) => <tr className={classes.tdatarow} key={`${ele.name}_${i}`}>
                        <td className={classes.tdata}>{ele?.job_id}</td>
                        <td className={classes.tdata}>{ele?.name}</td>
                        <td className={classes.tdata} >{ele?.role}</td>
                        <td className={classes.tdata}>{ele?.first_level_score}</td>
                        <td className={classes.tdata}>{ele?.second_level_score}</td>
                        <td className={classes.tdata}>{ele?.third_level_score}</td>
                        <td className={classes.tdata}>{ele?.fourth_level_score}</td>
                        <td className={classes.tdata}>{ele?.total_marks}</td>
                        <td className={classes.tdata}>{ele?.status}</td>
                        <td className={classes.tdata}>
                            <IconButton size="small">
                                <img src={Eye} alt="" className={classes.eyeIcon} />
                            </IconButton>
                        </td>
                    </tr>)}
                </tbody>
            </table>
        </div>
    </div>
}

export default CandidateTable;