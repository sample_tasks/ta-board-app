import { makeStyles } from "tss-react/mui";

const useStyles = makeStyles()((theme) => ({
    postedJobsContianer: {
        overflow: 'auto',
        boxShadow: "4px 4px 25px 0px #00000026",
        width: '100%',
        borderRadius: '5px'
    },
    titleCotainer: {
        display: 'flex',
        padding: '10px 20px',
        justifyContent: 'space-between'
    },
    leftCotainer: {
        display: 'flex',
        alignItems: 'center',
        gap: '20px'
    },
    rightCotainer: {
        display: 'flex',
        alignItems: 'center',
        gap: '10px',
        justifyContent: 'flex-end'
    },
    title: {
        fontFamily: "Poppins-Medium",
        fontSize: '18px',
        color: "#000000"
    },
    linkBtn: {
        color: '#0A66C2',
        backgroundColor: "#FFFFFF !important",
        borderRadius: "0px",
        padding: '2px',
        textTransform: 'capitalize',
        textDecoration: "underline",
        fontSize: "12px"
    },
    iconBtn: {
        width: '20px',
        height: '20px',
    },
    searchIcon: {
        width: '45%'
    },
    adornment: {
        height: '100%',
        margin: '0px',
        maxHeight: 'fit-content',
        alignItems: 'unset'
    },
    filterImg: {
        width: '15px',
        marginRight: "5px"
    },
    filterBtn: {
        height: "35px",
        color: "#000000",
        fontSize: "10px",
        textTransform: 'capitalize',
        boxShadow: "4px 4px 25px 0px #00000026",
    },
    eyeIcon: {
        width: "15px"
    },
    tableContainer: {
        padding: '20px',
        width: '100%'
    },
    table: {
        borderCollapse: "collapse",
        width: '100%'
    },
    tdatarow: {
        borderBottom: "1px solid #09131640",
        height: "50px",
    },
    theadrow: {
        borderBottom: "1px solid #000000",
        backgroundColor: "#F5F5F5",
        height: "50px",
    },
    theaddata: {
        color: "#3E3E3E",
        verticalAlign: "bottom",
        textAlign: 'left',
        fontSize: "12px",
        padding: '10px 10px 10px 0px',
        "&:first-child": {
            paddingLeft: "10px"
        },
    },
    tdata: {
        color: "#3E3E3E",
        verticalAlign: "bottom",
        textAlign: 'left',
        fontSize: "12px",
        padding: '10px 10px 10px 0px',
        "&:first-child": {
            paddingLeft: "10px"
        },
        "&:last-child": {
            textAlign: 'center',
        }
    }
}))

export default useStyles;