import { makeStyles } from "tss-react/mui";

const useStyles = makeStyles()((theme) => ({
    datePicker: {
        maxHeight: '280px',
        boxShadow: "4px 4px 25px 0px #00000026",
        width: '260px',
        overflow: 'unset',
        '& .MuiPickersCalendarHeader-root': {
            minHeight: 'fit-content',
            maxHeight: 'fit-content',
            margin: 0,
            marginTop: 10
        },
        '& .MuiPickersCalendarHeader-label': {
            fontSize: '14px'
        },
        '& .MuiDayCalendar-weekDayLabel': {
            width: 35,
            height: 35,
            padding: '0px 16px',
            margin: "0"
        },
        '& .MuiPickersDay-root': {
            width: 35,
            height: 35,
            padding: '0px 16px',
            margin: "0"
        },
        '& .MuiYearCalendar-root': {
            width: '260px',
            maxHeight: '280px',
            'button': {
                fontSize: '14px'
            }
        },
        '& .MuiDateCalendar-root': {
            maxHeight: '280px',
            width: '260px'
        },
        '& .MuiPickersCalendarHeader-root': {
            padding: ' 0px 12px'
        },
        '& .MuiPickersArrowSwitcher-button': {
            padding: '2px',
            marginRight: 0
        },
        '& .MuiPickersCalendarHeader-switchViewButton': {
            padding: 2
        },
        '& .MuiDayCalendar-slideTransition': {
            // minHeight: '200px',
        },
        '& .MuiPickersCalendarHeader-labelContainer': {
            overflow: 'unset'
        },
        '& .MuiPickersArrowSwitcher-spacer': {
            width: 10
        },
        fontFamily: 'Poppins-Regular'
    },
}))

export default useStyles;