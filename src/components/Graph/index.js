import * as React from 'react';
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Checkbox from '@mui/material/Checkbox';
import FormControlLabel from '@mui/material/FormControlLabel';
import { ChartContainer } from '@mui/x-charts/ChartContainer';
import { ResponsiveChartContainer } from '@mui/x-charts/ResponsiveChartContainer';
import { BarPlot } from '@mui/x-charts/BarChart';
import { LinePlot, MarkPlot } from '@mui/x-charts/LineChart';
import { ChartsXAxis } from '@mui/x-charts/ChartsXAxis';
import { ChartsYAxis } from '@mui/x-charts';
import { Divider } from '@mui/material';
import useStyles from './styles';

const dataset = [
    { bar: 2000, line: 1500, month: 'Jan' },
    { bar: 700, line: 1800, month: 'Feb' },
    { bar: 900, line: 2000, month: 'Mar' },
    { bar: 1100, line: 2300, month: 'Apr' },
    { bar: 1300, line: 1600, month: 'May' },
    { bar: 1500, line: 1900, month: 'Jun' },
    { bar: 2700, line: 1200, month: 'Jul' },
    { bar: 2900, line: 2400, month: 'Aug' },
    { bar: 2100, line: 3600, month: 'Sep' },
    { bar: 2300, line: 3800, month: 'Oct' },
    { bar: 2500, line: 2000, month: 'Nov' },
    { bar: 1700, line: 2200, month: 'Dec' }
];


export default function Graph() {
    const [isResponsive, setIsResponsive] = React.useState(false);
    const { classes } = useStyles();

    const Container = ResponsiveChartContainer;
    const sizingProps = {};
    const valueFormatter = (value) => `${value}`;
    return (<>
        <div className={classes.titleCotainer}>
            <div className={classes.leftCotainer}>
                <span className={classes.title}>Application’s  Info</span>
                <div className={classes.boxCotainer}>
                    <div className={classes.colorBox} style={{ backgroundColor: '#277ACC' }}></div>
                    <span className={classes.colorSpan}>Received</span>
                </div>
                <div className={classes.boxCotainer}>
                    <div className={classes.colorBox} style={{ backgroundColor: '#002B55BD' }}></div>
                    <span className={classes.colorSpan}>Processed</span>
                </div>
            </div>

            <select style={{ border: "1px solid #0A66C2" }}>
                <option>
                    Month
                </option>
                <option>
                    Week
                </option>
                <option>
                    Day
                </option>
            </select>
        </div>
        <Divider sx={{ margin: "0px 5px" }} />
        <Paper sx={{ width: '100%', height: 230, boxShadow: 'none', borderRadius: '10px' }} elevation={3}>
            <Container
                dataset={dataset}
                series={[
                    {
                        type: 'bar',
                        dataKey: 'bar',
                        yAxisKey: 'y-axis-id1',
                        color: '#277ACC'
                    },
                    {
                        type: 'line',
                        dataKey: 'line',
                        id: 'y-axis-id1',
                        showMark: false,
                        color: "#002B55BD"
                    },
                ]}
                xAxis={[
                    {
                        dataKey: 'month',
                        scaleType: 'band',
                        id: 'x-axis-id',
                        categoryGapRatio: 0.5
                    },
                ]}
                yAxis={[
                    {
                        id: 'y-axis-id1',
                    }
                ]}
            >
                <BarPlot />
                <LinePlot />
                <MarkPlot />
                <ChartsXAxis position="bottom" axisId="x-axis-id" />
                <ChartsYAxis
                    axisId="y-axis-id1"
                    position="left"
                />
            </Container>
        </Paper>
    </>
    );
}