import { makeStyles } from "tss-react/mui";

const useStyles = makeStyles()((theme) => ({
    titleCotainer: {
        display: 'flex',
        padding: '10px',
        justifyContent: 'space-between'
    },
    leftCotainer: {
        display: 'flex',
        [`${theme.breakpoints.down('sm')}`]: {
            flexFlow: 'column'
        },
    },
    boxCotainer: {
        display: 'flex',
        alignItems: 'center',
        marginLeft: '10px'
    },
    title: {
        fontFamily: "Poppins-Medium",
        fontSize: '14px',
        color: "#000000"
    },
    colorBox: {
        width: '10px',
        height: '10px'
    },
    colorSpan: {
        fontSize: '12px',
        color: '#000000',
        marginLeft: '5px'
    }
}))

export default useStyles;