import React from 'react';
import { ThemeProvider, createTheme, responsiveFontSizes, } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import { spanColor } from './utils/tools';

const lightTheme = createTheme({
    palette: {
        mode: 'light',
    },
    button: {
        textTransform: 'none',
    },
    typography: {
        "fontFamily": "Poppins-Regular",
        "fontSize": 14,
        "fontWeightLight": 300,
        "fontWeightRegular": 400,
        "fontWeightMedium": 500,
        color: spanColor,
        h5: {
            "fontFamily": "Poppins-SemiBold",
            "textTransform": "Capitalize"
        }
    },
});


const MUITheme = ({ children }) => {

    return (
        <ThemeProvider theme={responsiveFontSizes(lightTheme)}>
            <CssBaseline />
            {children}
        </ThemeProvider>
    );
};

export default MUITheme;